;ELC   
;;; Compiled
;;; in Emacs version 24.5.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#@102 Save a macro. Take a name as an argument and save the last defined macro under this name.

(fn NAME)
(defalias 'user--save-macro #[257 "\300!\210\301\302!\210db\210\303 \210\304!\210\303 \210\305 \210\306\307!\207" [kmacro-name-last-macro find-file "~/.emacs.d/macros.el" newline insert-kbd-macro save-buffer switch-to-buffer nil] 3 (#$ . 411) "SName of the macro :"])
#@101 Cleans the buffer by re-indenting, changing tabs to spaces, and removing trailing whitespace.

(fn)
(defalias 'user--clean-buffer #[0 "\300 \210\212\301\302\303\304ed%)\207" [delete-trailing-whitespace replace-regexp "^\n\\{3,\\}" "\n\n" nil] 6 (#$ . 789) nil])
#@42 Deletes the text inside of quotes.

(fn)
(defalias 'user--delete-in-quotes #[0 "\300\301\302 \"\210\303u\210`\304\301\305 \"\210\306u\210\307`\"\207" [search-backward-regexp "[\"']" line-beginning-position nil search-forward-regexp line-end-position -1 kill-region] 4 (#$ . 1057) nil])
#@44 Deletes the text within parentheses.

(fn)
(defalias 'user--delete-in-parentheses #[0 "\300\301\302 \"\210\303u\210`\304\305\306 \"\210\307u\210\310`\"\207" [search-backward "(" line-beginning-position nil search-forward ")" line-end-position -1 kill-region] 4 (#$ . 1350) nil])
#@84 Deletes the text within square brackets, angle brackets, and curly brackets.

(fn)
(defalias 'user--delete-in-brackets #[0 "\300\301\302 \"\210\303u\210`\304\305\306 \"\210\307u\210\310`\"\207" [search-backward-regexp "[[{<]" line-beginning-position nil search-forward-regexp "[]}>]" line-end-position -1 kill-region] 4 (#$ . 1636) nil])
#@80 Renames both current buffer and file it's visiting to NEW-NAME.

(fn NEW-NAME)
(defalias 'user--rename-this-file-and-buffer #[257 "\300 \301 \211\204 \302\303\"\210\304!\203 \305\306\"\202+ \307\310#\210\311!\210\312!\210\313\314!\207" [buffer-name buffer-file-name error "Buffer '%s' is not visiting a file!" get-buffer message "A buffer named '%s' already exists!" rename-file 1 rename-buffer set-visited-file-name set-buffer-modified-p nil] 7 (#$ . 1981) "sNew name: "])
#@71 Insert a time-stamp according to locale's date and time format.

(fn)
(defalias 'user--insert-date #[0 "\300\301\302 \"c\207" [format-time-string "%c" current-time] 3 (#$ . 2470) nil])
#@78 Creates a numbered list from provided start to provided end.

(fn START END)
(defalias 'user--generate-numbered-list #[514 "\211X\205 \300!\301\261\210\302 \210\211T\262\202 \207" [number-to-string "." newline] 5 (#$ . 2661) "nStart num:\nnEnd num:"])
#@80 Moves forward through the mark ring. Does nothing if mark ring is empty.

(fn)
(defalias 'user--mark-ring-forward #[0 "\205* \301\302 !B\302 \303!@p\223\210\304\305!\204 \306\307\310\"\210\311!\312\303!@!b\207" [mark-ring copy-marker mark-marker last mark t message "No marks set." nil nbutlast marker-position] 3 (#$ . 2926) nil])
#@89 Creates a temporary file in the system temp directory, for various purposes.

(fn NAME)
(defalias 'user--make-temp-file #[257 "\301!\210\302!\210\303P!\207" [temporary-file-directory generate-new-buffer switch-to-buffer write-file] 4 (#$ . 3272) "sFile name:"])
#@78 Search all open buffers for a regex. Open an occur-like window.

(fn REGEXP)
(defalias 'user--search-all-buffers #[257 "\300\301\302#\207" [multi-occur-in-matching-buffers "." t] 5 (#$ . 3544) "sRegexp: "])
#@104 Asks for confirmation before overwriting an existing register with a point-to-register.

(fn REGISTER)
(defalias 'user--safe-point-to-register #[257 "\300!\204\n \301!\207\302\303!\205 \301!\207" [get-register point-to-register y-or-n-p "Replace existing register?"] 3 (#$ . 3759) "cRegister:"])
#@103 Asks for confirmation before overwriting an existing register with a copy-to-register.

(fn REGISTER)
(defalias 'user--safe-copy-to-register #[257 "\300!\204 \301\302 \303 #\207\304\305!\205 \301\302 \303 #\207" [get-register copy-to-register region-beginning region-end y-or-n-p "Replace existing register?"] 5 (#$ . 4066) "cRegister:"])
#@119 Asks for confirmation before overwriting an existing register with a window-configuration-to-register.

(fn REGISTER)
(defalias 'user--safe-window-config-to-register #[257 "\300!\204\n \301!\207\302\303!\205 \301!\207" [get-register window-configuration-to-register y-or-n-p "Replace existing register?"] 3 (#$ . 4417) "cRegister:"])
#@50 Moves to the end of the line and newlines.

(fn)
(defalias 'user--end-of-line-newline #[0 "\300\210\301 \207" [nil newline] 1 (#$ . 4761) nil])
#@101 Scrolls the buffer `lines` per second, which allows for roughly skimming over a buffer.

(fn LINES)
(defalias 'user--skim-buffer #[257 "\300\301!\245\302\303!!`dU?\205\" \304!\210\211y\210\305\210\306!\210\202\n \207" [3 float floor sqrt scroll-up nil sit-for] 5 (#$ . 4913) "nLines per sec: "])
#@85 Converts the markdown used by the factorio changelog into mediawiki markdown.

(fn)
(defalias 'user--factorio-convert-changelog #[0 "\300b\210\301\302!\210\300b\210\303\304\305\"\210\300b\210\303\306\307\"\210\300b\210\303\310\307\"\210\300b\210\303\311\312\"\210\300b\210\303\313\314\"\210\300b\210\303\315\316\"\210\300b\210\303\317\320\"\210\300b\210\303\321\320\"\210\300b\210\303\322\323\"\210\300b\210\303\324\323\"\210\300b\210\303\325\326\"\210\300b\207" [0 flush-lines "-----------" replace-regexp "^Version: \\([0-9]\\.[0-9]+\\.[0-9]+\\)" "== \\1 ==" "^\\s-+\\(Balancing\\|\\(Minor\\|Major\\|Small\\)? [fF]eatures\\|Changes\\|Sounds?\\|Circuit [nN]etwork\\|Modding\\|Scripting\\|Bug[fF]ixes\\):" "=== \\1 ===" "^\\s-+\\(Graphics\\|Optimi[zs]ations\\|Configuration\\|Locale\\|Command line interface\\):" "^\\s-+-" "*" "^\\n\\s-+\\([a-z]\\)" " \\1" "^\\s-+\\([A-Z]\\)" "** \\1" "(?\\(https?://\\(www\\.\\)?forums\\.factorio\\.com/[0-9]+\\))?" "([\\1 more])" "(?\\(https?://\\(www\\.\\)?factorio\\.com/blog/post/fff-[0-9]+\\))?" "\\w+::\\(\\w+_?\\)+(?)?" "<code>\\&</code>" "--\\(\\w+-?\\)+" "\\s-/\\(\\w+-?\\)+" " <code>\\&</code>"] 3 (#$ . 5220) nil])
#@71 Set font to a monospaced font for programming in current buffer

(fn)
(defalias 'user--buffer-font-monospace #[0 "\301\302 \207" [buffer-face-mode-face (:family "Terminus (TTF)" :foundry "PfEd" :slant normal :weight normal :height 122 :width normal) buffer-face-mode] 1 (#$ . 6387) nil])
#@59 Copy the current buffer file name to the clipboard.

(fn)
(defalias 'user--copy-file-name-to-clipboard #[0 "\302\232\203\n 	\202\f \303 \211\205 \304!\210\305\306\"\207" [major-mode default-directory dired-mode buffer-file-name kill-new message "Copied buffer file name '%s' to the clipboard."] 4 (#$ . 6682) nil])
#@123 Same as `find-tag-other-window' but doesn't move the point to the created window.

(fn TAGNAME &optional NEXT-P REGEXP-P)
(defalias 'user--view-tag-other-window #[769 "\300 \301#\210\302\303!\210\304!\207" [get-buffer-window find-tag-other-window recenter 0 select-window] 8 (#$ . 7008) (find-tag-interactive "Tag: ")])
