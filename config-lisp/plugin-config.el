(require 'use-package)

;;Avy
(use-package avy
  :bind (("C-;" . avy-goto-char-timer)
         ("C-'" . avy-kill-region)
         ("C-:" . avy-kill-whole-line)
         ("M-z" . avy-zap-up-to-char)))

;; Flyspell
(eval-after-load 'flyspell '(define-key flyspell-mode-map (kbd "C-;") nil)) ;;disables the binding so avy can use it

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Smex
(use-package smex
  :init (setq smex-save-file (expand-file-name ".smex-items" user-emacs-directory))
  :bind (("M-x" . smex)
         ("M-X" . smex-major-mode-commands))
  :config (smex-initialize))

;; Windmove
(require 'windmove)
(windmove-default-keybindings)

;;lua mode
(use-package lua-mode
  :config (setq lua-indent-level 4))

;; Smart mode line
(use-package smart-mode-line
  :init (progn
          (setq sml/theme 'dark)
          (sml/setup)))

;; Flycheck
(use-package flycheck
  :hook (prog-mode . flycheck-mode)
  :config (progn
            (setq-default flycheck-gcc-language-standard "c++14") ;; Check with 2014 standard
            (setq-default flycheck-clang-language-standard "c++14"))) ;; Check with 2014 standard

;; Company
(require 'company)
(global-company-mode 1)
(global-set-key (kbd "<tab>") 'company-complete-common-or-cycle)
(setq company-clang-executable "/usr/bin/clang-6.0")
(setq company-idle-delay 2)


;; Eshell
(setq eshell-prompt-function
      (lambda ()
        (concat
         (propertize "┌─[" 'face `(:foreground "black"))
         (propertize (user-login-name) 'face `(:foreground "red"))
         (propertize "@" 'face `(:foreground "black"))
         (propertize (system-name) 'face `(:foreground "blue"))
         (propertize "]──[" 'face `(:foreground "black"))
         (propertize (format-time-string "%H:%M" (current-time)) 'face `(:foreground "blue"))
         (propertize "]──[" 'face `(:foreground "black"))
         (propertize (concat (eshell/pwd)) 'face `(:foreground "dark purple"))
         (propertize "]\n" 'face `(:foreground "black"))
         (propertize "└─>" 'face `(:foreground "black"))
         (propertize (if (= (user-uid) 0) " # " " $ ") 'face `(:foreground "black"))
         )))

;; Ibuffer
(eval-after-load 'ibuffer
  '(progn
     (setq ibuffer-show-empty-filter-groups nil)
     (setq ibuffer-saved-filter-groups
           (quote (("Standard"
                    ("ELisp" (mode . emacs-lisp-mode))
                    ("Text" (or (mode . text-mode)
                                (mode . markdown-mode)
                                (mode . org-mode)
                                (mode . mediawiki-mode)))
                    ("Dired" (mode . dired-mode))
                    ("Configuration" (or (mode . conf-mode)
                                         (mode . conf-space-mode)
                                         (mode . conf-unix-mode)
                                         (mode . conf-colon-mode)
                                         (mode . conf-windows-mode)
                                         (mode . conf-xdefaults-mode)))
                    ("Scripting" (or (mode . makefile-mode)
                                     (mode . shell-script-mode)
                                     (mode . makefile-gmake-mode)))
                    ("Programming" (or (mode . javascript-mode)
                                       (mode . lua-mode)
                                       (mode . c-mode)
                                       (mode . c++-mode)))))))))

;; Org
(eval-after-load 'org
  '(progn
     (define-key org-mode-map [tab] 'company-complete-common-or-cycle) ;; Set this, because I use tab complete much more than cycle
     (setq org-log-done t)))
