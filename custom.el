(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(avy-timeout-seconds 0.5)
 '(confirm-kill-emacs (quote yes-or-no-p))
 '(custom-enabled-themes (quote (smart-mode-line-dark)))
 '(custom-safe-themes
   (quote
    ("1b54ddc20c577c1abc57c27cfcb3eded10d3ddb9a4e1cd632217ccb9b937d664" "d8cec8251169ccfe192aa87d69b9378bc81599330f31498f85deaef633721302" "ef98b560dcbd6af86fbe7fd15d56454f3e6046a3a0abd25314cfaaefd3744a9e" "76dc63684249227d64634c8f62326f3d40cdc60039c2064174a7e7a7a88b1587" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(frame-background-mode (quote light))
 '(ispell-personal-dictionary "~/.emacs.d/dictionary")
 '(save-place t nil (saveplace))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Serif" :foundry "PfEd" :slant normal :weight normal :height 107 :width normal))))
 '(rainbow-delimiters-depth-1-face ((t (:foreground "black"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "saddle brown"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "dark green"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "dark blue"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "dark violet"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "dark cyan")))))
